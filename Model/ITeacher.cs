﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
   public interface ITeacher
    {
        public int? TeacherId { get; set; }
    }
}
