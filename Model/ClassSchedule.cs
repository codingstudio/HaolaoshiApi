﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    /// <summary>
    /// 班级课表
    /// </summary>
    [Serializable]
    [Table("ClassSchedule")]
    public class ClassSchedule : SchoolUserTeacherID
    {
        /// <summary>
        /// 班级编号
        /// </summary>
        [Required(ErrorMessage = "教室编号必填")]
        public string ClassroomSn { get; set; }
        public int? ClassroomId { get; set; }
        [ForeignKey("ClassroomId")]
        public virtual Classroom Classroom { get; set; }

        [Required(ErrorMessage = "课时编号必填")]
        public string TimeTableSn { get; set; }
        public int? TimeTableId { get; set; } 
        [ForeignKey("TimeTableId")]
        public virtual TimeTable TimeTable { get; set; }

        //[Required(ErrorMessage = "班级编号必填")]
        public int? ClasssId { get; set; }
        [ForeignKey("ClasssId")]
        [Display(Name = "班级")] 
        public virtual Classs Classs { get; set; }
        [Required(ErrorMessage = "日期必填")]
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date{ get; set; }       

        [Required(ErrorMessage = "教师编号必填")]
        public string TeacherSn { get; set; }//对应教室排课编号
        //public int? TeacherId { get; set; }
        //[ForeignKey("TeacherId")]
        //public virtual Teacher Teacher { get; set; }

        //[Required(ErrorMessage = "课程编号必填")]
        public string CourseSn { get; set; }//对应教室排课编号
        public int? CourseId { get; set; }
        [ForeignKey("CourseId")]  
        [Display(Name = "课程")]
        public virtual Course Course { get; set; }
        /// <summary>
        /// 课程内容
        /// </summary>
        public string Content { get; set; }
    }
}
