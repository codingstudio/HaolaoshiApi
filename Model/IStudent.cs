﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
   public interface IStudent
    {
        public int? StudentId { get; set; }
    }
}
