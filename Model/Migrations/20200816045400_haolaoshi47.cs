﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi47 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Student_class",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Student_name",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Interview_time",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Interview_way",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Num",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Post",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Resume_time",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Salary",
                table: "Company");

            migrationBuilder.AddColumn<DateTime>(
                name: "Interview_time",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Interview_way",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Num",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Resume_time",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Salary",
                table: "Post",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Interview_time",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Interview_way",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Num",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Resume_time",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Salary",
                table: "Post");

            migrationBuilder.AddColumn<string>(
                name: "Student_class",
                table: "Interview",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Student_name",
                table: "Interview",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "Interview_time",
                table: "Company",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Interview_way",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Num",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Post",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Resume_time",
                table: "Company",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Salary",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
