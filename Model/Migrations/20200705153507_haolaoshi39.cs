﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi39 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_User_UserId",
                table: "Area");

            migrationBuilder.DropIndex(
                name: "IX_Area_UserId",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Area");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Area",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Area_UserId",
                table: "Area",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_UserId",
                table: "Area",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
