﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi56 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AbsenceId",
                table: "SchoolConfig",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClockAheadTime",
                table: "SchoolConfig",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClockExpireTime",
                table: "SchoolConfig",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LateId",
                table: "SchoolConfig",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SchoolConfig_AbsenceId",
                table: "SchoolConfig",
                column: "AbsenceId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolConfig_LateId",
                table: "SchoolConfig",
                column: "LateId");

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolConfig_UsualScoreItem_AbsenceId",
                table: "SchoolConfig",
                column: "AbsenceId",
                principalTable: "UsualScoreItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolConfig_UsualScoreItem_LateId",
                table: "SchoolConfig",
                column: "LateId",
                principalTable: "UsualScoreItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SchoolConfig_UsualScoreItem_AbsenceId",
                table: "SchoolConfig");

            migrationBuilder.DropForeignKey(
                name: "FK_SchoolConfig_UsualScoreItem_LateId",
                table: "SchoolConfig");

            migrationBuilder.DropIndex(
                name: "IX_SchoolConfig_AbsenceId",
                table: "SchoolConfig");

            migrationBuilder.DropIndex(
                name: "IX_SchoolConfig_LateId",
                table: "SchoolConfig");

            migrationBuilder.DropColumn(
                name: "AbsenceId",
                table: "SchoolConfig");

            migrationBuilder.DropColumn(
                name: "ClockAheadTime",
                table: "SchoolConfig");

            migrationBuilder.DropColumn(
                name: "ClockExpireTime",
                table: "SchoolConfig");

            migrationBuilder.DropColumn(
                name: "LateId",
                table: "SchoolConfig");
        }
    }
}
