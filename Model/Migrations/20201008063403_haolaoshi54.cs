﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi54 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "UsualScoreLog",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "UsualScoreItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "UsualScore",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "TimeTable",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Teacher",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Student",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Slider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Score",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "School",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "RoleAuthority",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Role",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Res",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Post",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "OnlineResume",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Major",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Interview",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Images",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Group",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Grade",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "CourseRes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "CourseClass",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Course",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Company",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "College",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "ClockLog",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Clock",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "ClassSchedule",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Classs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Classroom",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Chapter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Category",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "AuthorityRes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Authority",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Article",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Area",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "School");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "CourseRes");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "CourseClass");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "College");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Chapter");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Area");
        }
    }
}
