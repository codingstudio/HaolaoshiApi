﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //章节
    [Serializable]
    [Table("Chapter")]
    public class Chapter : SchoolUserStudentTeacherTreeID<Chapter>, IFee
    {    
        [Display(Name = "名称")]
        public string Name { get; set; }
        public int? CourseId { get; set; } 
        [ForeignKey("CourseId")]
        [Display(Name = "课程")]
        public virtual Course Course { get; set; }
        [Display(Name = "封面")]
        public string Pic { get; set; }      
        [Display(Name = "简介")]
        public string Intro { get; set; }
        [Display(Name = "内容")]
        public string Detail { get; set; }
        /// <summary>
        /// 附件，多个用“;”隔开
        /// </summary>
        [Display(Name = "附件")]
        public string Attach { get; set; }
        /// <summary>
        /// 金币
        /// </summary>
        public int Gold { get; set; }
        /// <summary>
        /// 启用章节背景。默认背景绿色。值不为空表示启用
        /// </summary>
        public string Bg { get; set; }
    }
}