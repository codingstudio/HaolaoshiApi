﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //学校配置
    [Serializable]
    [Table("SchoolConfig")]
    public class SchoolConfig : SchoolUserID
    {
        /// <summary>
        /// 系统默认平时表现成绩分数70
        /// </summary>
        public double UsualScoreScore { get; set; } = 70;
        public int? AbsenceId { get; set; }
        /// <summary>
        /// 缺席旷课关联平时表现成绩项
        /// </summary>
        [ForeignKey("AbsenceId")]
        public virtual UsualScoreItem Absence { get; set; }
        public int? LateId { get; set; }
        /// <summary>
        /// 迟到关联平时表现成绩项
        /// </summary>
        [ForeignKey("LateId")]
        public virtual UsualScoreItem Late { get; set; }
        /// <summary>
        /// 签到提前时间，单位分钟
        /// </summary>
        public int ClockAheadTime { get; set; } = 20;
        /// <summary>
        ///  签到延时时间，单位分钟
        /// </summary>
        public int ClockExpireTime { get; set; } = 30;
    }
}