﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace Bll
{
   
    public interface IUsualScoreBll:IBaseBll<UsualScore>
    {
        /// <summary>
        /// 查询或初始化课程对应班级学生的平时成绩。没有则初始化。按学生编号升序排列
        /// </summary>
        /// <param name="CourseId"></param>
        /// <param name="ClasssId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<UsualScore> SelectAllOrInit(int CourseId, int ClasssId, int schoolId, int userId);
    }
}
