using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class ClockBll : BaseBll<Clock>, IClockBll
    {
        public ClockBll(IClockDAL dal):base(dal)
        {
        }
    }
}
