using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class CourseClassBll : BaseBll<CourseClass>, ICourseClassBll
    {
        public CourseClassBll(ICourseClassDAL dal):base(dal)
        {
        }
    }
}
