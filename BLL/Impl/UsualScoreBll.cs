﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class UsualScoreBll : BaseBll<UsualScore>, IUsualScoreBll
    {
        public IStudentBll studentBll { get; set; }
        public IClasssBll classsBll { get; set; }
        public IUsualScoreLogBll usualScoreLogBll { get; set; }
        public IUsualScoreItemBll usualScoreItemBll { get; set; }
        public ISchoolConfigBll schoolConfigBll { get; set; }
        public UsualScoreBll(IUsualScoreDAL dal) : base(dal)
        {
        }
        public IEnumerable<UsualScore> SelectAllOrInit(int CourseId, int ClasssId, int schoolId, int userId)
        {
            IEnumerable<UsualScore> objs = this.SelectAll(o => o.CourseId == CourseId && o.Student.ClasssId == ClasssId);
            if (!objs.GetEnumerator().MoveNext())//没有记录 初始化成绩信息
            {
                IEnumerable<Student> students = studentBll.SelectAll(o => o.ClasssId == ClasssId);//查询学生
                //获取学校配置参数
                SchoolConfig schoolConfig = schoolConfigBll.SelectOneBySchoolOrInit(schoolId);
                List<UsualScore> usualScores = new List<UsualScore>();
                foreach (Student s in students)
                {
                    //int? schoolId = classsBll.SelectOne(s.ClasssId)?.SchoolId;//获取学校
                    //初始化学生成绩
                    usualScores.Add(new UsualScore() { SchoolId = s.Classs?.SchoolId, StudentId = s.Id, Student = s, CourseId = CourseId, Score = schoolConfig.UsualScoreScore, Sys = false });
                }
                this.DbContext().Database.BeginTransaction();//开启事务
                bool ret = this.Add(usualScores);
                this.DbContext().Database.CommitTransaction();
                return usualScores;
            }
            return objs;
        }
    }
}
