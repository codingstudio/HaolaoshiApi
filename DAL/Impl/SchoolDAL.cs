using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class SchoolDAL : BaseDAL<School>, ISchoolDAL
    {
        public SchoolDAL(MyDbContext db) : base(db)
        {
        }
    }
}
