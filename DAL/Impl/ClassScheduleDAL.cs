using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class ClassScheduleDAL : BaseDAL<ClassSchedule>, IClassScheduleDAL
    {
        public ClassScheduleDAL(MyDbContext db) : base(db)
        {
        }
    }
}
